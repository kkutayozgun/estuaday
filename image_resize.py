#!/usr/bin/python
from PIL import Image
import os, sys





def resize(path):
    dirs = os.listdir(path)
    for item in dirs:
        if os.path.isfile(path + item):
            im = Image.open(path + item)
            f, e = os.path.splitext(path + item)

            im.save(f + '.jpg', 'JPEG', quality=30)

paths = [
    "./media/altbirimler/", "./media/myobolum/"
]
for path in paths:
    resize(path)
