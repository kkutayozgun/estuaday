from django.urls import path
from . import views

app_name = "siteapp"

urlpatterns =[
    path('fakulteler/', views.FakulteListView.as_view(), name="fakulte_list"),
    path('fakulteler/<str:pk>/bolumler/', views.FakulteBolumListView.as_view(), name="fakulte_bolum_list"),
    path('bolum/<str:pk>/detail', views.BolumDetailView.as_view(), name="bolum_detail"),

    path('bolumler/', views.BolumListView.as_view(), name="bolum_list"),

    path('enstituler/', views.EnstituListView.as_view(), name="enstitu_list"),
    path('enstituler/<str:pk>/anabilimdali/', views.EnstituAnabilimDaliListView.as_view(), name="enstitu_abd_list"),
    path('anabilimdali/<str:pk>/detail', views.AnabilimDaliDetailView.as_view(), name="abd_detail"),

    path('kulupler/', views.KulupListView.as_view(), name="kulup_list"),
    path('kulup/<str:pk>/detail', views.KulupDetailView.as_view(), name="kulup_detail"),

    path('ogrenciprojeler/', views.OgrenciProjeListView.as_view(), name="ogrenciproje_list"),
    path('ogrenciproje/<str:pk>/detail', views.OgrenciProjeDetailView.as_view(), name="ogrenciproje_detail"),

    path('myo/', views.MYOListView.as_view(), name="myo_list"),
    path('myo/<str:pk>/bolumler/', views.MYOBolumListView.as_view(), name="myo_bolum_list"),
    path('myo/bolum/<str:pk>/detail', views.MYOBolumDetailView.as_view(), name="myo_bolum_detail"),

    path('yorumlar/', views.YorumListView.as_view(), name="yorum_list"),
    path('kampusteyasam/', views.KampusteYasamListView.as_view(), name="kampuste_yasam"),
    path('eskisehirdeyasam/', views.EskisehirdeYasamTemplateView.as_view(), name="eskisehirde_yasam"),
    path('tercihgunleri/', views.TercihGunleriTemplateView.as_view(), name="tercih_gunleri"),
    path('tanitimfilmleri/', views.TanitimFilmleriTemplateView.as_view(), name="tanitim_filmleri"),

    path('puanlar/', views.PuanlarTemplateView.as_view(), name="puanlar"),

    path('estudeneyimle/', views.EstuDeneyimleTemplateView.as_view(), name="estu_deneyimle"),
    path('arastirmalisansustu/', views.EstuArastirmaLisansustuEgitimTemplateView.as_view(), name="estu_arastirma_lisansustu"),
]