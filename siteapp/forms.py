from django import forms
from .models import *


class YorumFilterForm(forms.Form):
    fakulte = forms.ModelChoiceField(queryset=Fakulte.objects.all(), required=False)
    bolum = forms.ModelChoiceField(queryset=Bolum.objects.all(), required=False)
    gorev = forms.ChoiceField(choices=YORUM_CHOICES, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for f in self.fields.keys():
            self.fields[f].widget.attrs.update({'class':'form-control'})

        self.fields['fakulte'].label = "Fakülte"
        self.fields['bolum'].label = "Bölüm"
        self.fields['gorev'].label = "Öğrenci / Mezun / Öğretim Üyesi"