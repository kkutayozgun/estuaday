from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from .models import *


# Create your views here.

class FakulteListView(ListView):
    model = Fakulte
    template_name = "siteapp/fakulte/fakulteler.html"
    context_object_name = "fakulteler"


class FakulteBolumListView(DetailView):
    model = Fakulte
    template_name = "siteapp/fakulte/fakulte_bolumler.html"
    context_object_name = "active_fakulte"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fakulteler'] = Fakulte.objects.all()
        return context


class BolumDetailView(DetailView):
    model = Bolum
    template_name = "siteapp/fakulte/bolum_detail.html"
    context_object_name = "active_bolum"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fakulteler'] = Fakulte.objects.all()
        return context


class EnstituListView(ListView):
    model = Enstitu
    template_name = "siteapp/enstitu/enstituler.html"
    context_object_name = "enstituler"


class EnstituAnabilimDaliListView(DetailView):
    model = Enstitu
    template_name = "siteapp/enstitu/enstitu_anabilimdallari.html"
    context_object_name = "active_enstitu"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['enstituler'] = Enstitu.objects.all()
        return context


class AnabilimDaliDetailView(DetailView):
    model = AnaBilimDali
    template_name = "siteapp/enstitu/anabilimdali_detail.html"
    context_object_name = "active_abd"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['enstituler'] = Enstitu.objects.all()
        return context


class KulupListView(ListView):
    model = Kulup
    template_name = "siteapp/kulup/kulupler.html"
    context_object_name = "kulupler"


class KulupDetailView(DetailView):
    model = Kulup
    template_name = "siteapp/kulup/kulup_detail.html"
    context_object_name = "active_kulup"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['kulupler'] = Kulup.objects.all()
        return context


class BolumListView(ListView):
    model = Bolum
    template_name = "siteapp/bolum_list.html"
    context_object_name = "bolumler"


class MYOListView(ListView):
    model = MeslekYO
    template_name = "siteapp/myo/meslekyo.html"
    context_object_name = "myolar"


class MYOBolumListView(DetailView):
    model = MeslekYO
    template_name = "siteapp/myo/myo_bolumler.html"
    context_object_name = "active_myo"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['myolar'] = MeslekYO.objects.all()
        return context


class MYOBolumDetailView(DetailView):
    model = MYOBolum
    template_name = "siteapp/myo/myo_bolum_detail.html"
    context_object_name = "active_bolum"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['myolar'] = MeslekYO.objects.all()
        return context


class YorumListView(ListView):
    model = Yorum
    template_name = "siteapp/yorum_list.html"
    context_object_name = "yorumlar"
    paginate_by = 18

    def get_queryset(self):
        queryset = Yorum.objects.all()

        if self.request.GET.get('fakulte'):
            fakulte = Fakulte.objects.get(pk=self.request.GET.get('fakulte'))
            queryset = queryset.filter(fakulte=fakulte)
        if self.request.GET.get('bolum'):
            bolum = Bolum.objects.get(pk=self.request.GET.get('bolum'))
            queryset = queryset.filter(bolum=bolum)
        if self.request.GET.get('gorev'):
            queryset = queryset.filter(gorev=self.request.GET.get('gorev'))

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fakulteler'] = Fakulte.objects.all()
        context['bolumler'] = Bolum.objects.all()
        context['gorevler'] = YORUM_CHOICES
        return context


class KampusteYasamListView(ListView):
    model = KampusGaleri
    template_name = "siteapp/kampuste_yasam.html"
    context_object_name = "resimler"


class PuanlarTemplateView(TemplateView):
    template_name = "siteapp/puanlar.html"


class OgrenciProjeListView(ListView):
    model = OgrenciProje
    template_name = "siteapp/ogrenciproje/ogrenciprojeler.html"
    context_object_name = "ogrenciprojeler"


class OgrenciProjeDetailView(DetailView):
    model = OgrenciProje
    template_name = "siteapp/ogrenciproje/ogrenciproje_detail.html"
    context_object_name = "active_proje"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ogrenciprojeler'] = OgrenciProje.objects.all()
        return context

    
class EskisehirdeYasamTemplateView(TemplateView):
    template_name = "siteapp/eskisehirde_yasam.html"


class TercihGunleriTemplateView(TemplateView):
    template_name = "siteapp/tercih_gunleri.html"


class TanitimFilmleriTemplateView(TemplateView):
    template_name = "siteapp/tanitim_filmleri.html"

class EstuDeneyimleTemplateView(TemplateView):
    template_name = "siteapp/estu_deneyimle.html"

class EstuArastirmaLisansustuEgitimTemplateView(TemplateView):
    template_name = "siteapp/estu_arastirma_lisansustu_egitim.html"