import pandas as pd
from django.core.management.base import BaseCommand

from siteapp.models import *


def convert_df(func):
    def inner():
        data = func()
        return pd.DataFrame(data)

    return inner


def get_value(obj, key, default=""):
    val = getattr(obj, key, default)
    if val:
        return str(val)
    return default


class Command(BaseCommand):
    help = "Backup database to file"

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str, help="filename to backup")

    def handle(self, *args, **kwargs):
        filename = kwargs.get('filename')

        fakulte_data = self.backup_fakulte()
        bolum_data = self.backup_bolum()
        enstitu_data = self.backup_enstitu()
        anabilimdali_data = self.backup_anabilimdali()
        kulup_data = self.backup_kulup()
        yorum_data = self.backup_yorum()
        meslekyo_data = self.backup_meslekyo()
        myobolum_data = self.backup_myobolum()
        kampusgaleri_data = self.backup_kampusgaleri()
        ogrenciproje_data = self.backup_ogrenciproje()

        writer = pd.ExcelWriter(filename)

        fakulte_data.to_excel(writer, sheet_name='fakulte')
        bolum_data.to_excel(writer, sheet_name='bolum')
        enstitu_data.to_excel(writer, sheet_name='enstitu')
        anabilimdali_data.to_excel(writer, sheet_name='anabilimdali')
        kulup_data.to_excel(writer, sheet_name='kulup')
        yorum_data.to_excel(writer, sheet_name='yorum')
        meslekyo_data.to_excel(writer, sheet_name='meslekyo')
        myobolum_data.to_excel(writer, sheet_name='myobolum')
        kampusgaleri_data.to_excel(writer, sheet_name='kampusgaleri')
        ogrenciproje_data.to_excel(writer, sheet_name='ogrenciproje')

        writer.save()

    @staticmethod
    @convert_df
    def backup_fakulte():
        data = []
        for fakulte in Fakulte.objects.all():
            data.append({
                'baslik': get_value(fakulte, 'baslik'),
                'kapakfoto': get_value(fakulte, 'kapakfoto'),
                'sunum': get_value(fakulte, 'sunum')
            })
        return data

    @staticmethod
    @convert_df
    def backup_bolum():
        data = []
        for bolum in Bolum.objects.all():
            data.append({
                'baslik': get_value(bolum, 'baslik'),
                'tur': bolum.get_tur_display(),
                'fakulte': get_value(bolum, 'bolum'),
                'yazi': get_value(bolum, 'yazi'),
                'resim': get_value(bolum, 'resim'),
                'thumbnail': get_value(bolum, 'thumbnail'),
                'sunum': get_value(bolum, 'sunum'),
                'telefon': get_value(bolum, 'telefon'),
                'bolum_mail': get_value(bolum, 'bolum_mail'),
                'fakulte_mail': get_value(bolum, 'fakulte_mail'),
                'web_sitesi': get_value(bolum, 'web_sitesi'),
                'facebook_hesap': get_value(bolum, 'facebook_hesap'),
                'instagram_hesap': get_value(bolum, 'instagram_hesap'),
                'twitter_hesap': get_value(bolum, 'twitter_hesap'),
                'youtube_hesap': get_value(bolum, 'youtube_hesap'),
                'linkedin_hesap': get_value(bolum, 'linkedin_hesap'),
                'youtube_video': get_value(bolum, 'youtube_video'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_enstitu():
        data = []
        for enstitu in Enstitu.objects.all():
            data.append({
                'baslik': get_value(enstitu, 'baslik'),
                'sunum': get_value(enstitu, 'sunum'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_anabilimdali():
        data = []
        for abd in AnaBilimDali.objects.all():
            data.append({
                'baslik': get_value(abd, 'baslik'),
                'tur': abd.get_tur_display(),
                'bolum': get_value(abd, 'bolum'),
                'yazi': get_value(abd, 'yazi'),
                'resim': get_value(abd, 'resim'),
                'sunum': get_value(abd, 'sunum'),
                'telefon': get_value(abd, 'telefon'),
                'anabilimdali_mail': get_value(abd, 'anabilimdali_mail'),
                'web_sitesi': get_value(abd, 'web_sitesi'),
                'enstitu_mail': get_value(abd, 'enstitu_mail'),
                'facebook_hesap': get_value(abd, 'facebook_hesap'),
                'instagram_hesap': get_value(abd, 'instagram_hesap'),
                'twitter_hesap': get_value(abd, 'twitter_hesap'),
                'youtube_hesap': get_value(abd, 'youtube_hesap'),
                'linkedin_hesap': get_value(abd, 'linkedin_hesap'),
                'youtube_video': get_value(abd, 'youtube_video'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_kulup():
        data = []
        for kulup in Kulup.objects.all():
            data.append({
                'baslik': get_value(kulup, 'baslik'),
                'yazi': get_value(kulup, 'yazi'),
                'paragraph': get_value(kulup, 'paragraph'),
                'resim': get_value(kulup, 'resim'),
                'logo': get_value(kulup, 'logo'),
                'kulup_baskani': get_value(kulup, 'kulup_baskani'),
                'kulup_danismani': get_value(kulup, 'kulup_danismani'),
                'kulup_danismani_link': get_value(kulup, 'kulup_danismani_link'),
                'kulup_mail': get_value(kulup, 'kulup_mail'),
                'web_sitesi': get_value(kulup, 'web_sitesi'),
                'facebook_hesap': get_value(kulup, 'facebook_hesap'),
                'instagram_hesap': get_value(kulup, 'instagram_hesap'),
                'twitter_hesap': get_value(kulup, 'twitter_hesap'),
                'youtube_hesap': get_value(kulup, 'youtube_hesap'),
                'linkedin_hesap': get_value(kulup, 'linkedin_hesap'),
                'youtube_video': get_value(kulup, 'youtube_video'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_yorum():
        data = []

        for yorum in Yorum.objects.all():
            data.append({
                'adsoyad': get_value(yorum, 'adsoyad'),
                'yazi': get_value(yorum, 'yazi'),
                'rol': get_value(yorum, 'rol'),
                'gorev': yorum.get_gorev_display(),
                'fakulte': get_value(yorum, 'fakulte'),
                'bolum': get_value(yorum, 'bolum'),
                'resim': get_value(yorum, 'resim'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_meslekyo():
        data = []
        for myo in MeslekYO.objects.all():
            data.append({
                'baslik': get_value(myo, 'baslik'),
                'kapakfoto': get_value(myo, 'kapakfoto'),
                'sunum': get_value(myo, 'sunum'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_myobolum():
        data = []
        for myo in MYOBolum.objects.all():
            data.append({
                'baslik': get_value(myo, 'baslik'),
                'tur': myo.get_tur_display(),
                'myo': get_value(myo, 'myo'),
                'yazi': get_value(myo, 'yazi'),
                'resim': get_value(myo, 'resim'),
                'thumbnail': get_value(myo, 'thumbnail'),
                'sunum': get_value(myo, 'sunum'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_kampusgaleri():
        data = []
        for galeri in KampusGaleri.objects.all():
            data.append({
                'baslik': get_value(galeri, 'baslik'),
                'resim': get_value(galeri, 'resim'),
            })
        return data

    @staticmethod
    @convert_df
    def backup_ogrenciproje():
        data = []
        for proje in OgrenciProje.objects.all():
            data.append({
                'baslik': get_value(proje, 'baslik'),
                'yazi': get_value(proje, 'yazi'),
                'resim': get_value(proje, 'resim'),
            })
