from django.contrib import admin
from .models import *

# Register your models here.


admin.site.register(Fakulte)
admin.site.register(Bolum)
admin.site.register(Enstitu)
admin.site.register(AnaBilimDali)
admin.site.register(Kulup)
admin.site.register(Yorum)
admin.site.register(MeslekYO)
admin.site.register(MYOBolum)
admin.site.register(KampusGaleri)
admin.site.register(OgrenciProje)
admin.site.register(MezunSesleniyorVideo)
admin.site.register(OgrenciProjeSesleniyorVideo)
admin.site.register(OgrenciTemsilciSesleniyorVideo)