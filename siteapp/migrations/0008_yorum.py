# Generated by Django 2.2 on 2019-07-21 22:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('siteapp', '0007_auto_20190716_1358'),
    ]

    operations = [
        migrations.CreateModel(
            name='Yorum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('adsoyad', models.CharField(max_length=100)),
                ('yazi', models.TextField(blank=True, null=True)),
                ('rol', models.CharField(blank=True, max_length=100, null=True)),
                ('gorev', models.CharField(blank=True, choices=[('ogretimuyesi', 'Öğretim Üyesi'), ('mezun', 'Mezun'), ('ogrenci', 'Öğrenci')], max_length=20, null=True)),
                ('resim', models.ImageField(blank=True, null=True, upload_to='yorumlar')),
                ('fakulte', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='siteapp.Fakulte')),
            ],
        ),
    ]
