# Generated by Django 2.2 on 2020-07-06 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('siteapp', '0017_auto_20200707_0104'),
    ]

    operations = [
        migrations.AddField(
            model_name='anabilimdali',
            name='telefon',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='bolum',
            name='telefon',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
