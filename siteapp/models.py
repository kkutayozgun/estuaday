import sys
from django.db import models
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _
# Create your models here.

BIRIM_TUR_CHOICES = (
    ('fakulte', 'Fakülte'),
    ('enstitu', 'Enstitü'),
    ('kulup', 'Kulüp'),
)

ALTBIRIM_TUR_CHOICES = (
    ('sayisal', 'Sayısal'),
    ('esitagirlik', 'Eşit Ağırlık'),
    ('sozel', 'Sözel'),
)


def compressImage(uploadedImage):
    imageTemproary = Image.open(uploadedImage)
    outputIoStream = BytesIO()
    # imageTemproaryResized = imageTemproary.resize((1020, 573))
    imageTemproary.save(outputIoStream, format='JPEG', quality=30)
    outputIoStream.seek(0)
    uploadedImage = InMemoryUploadedFile(outputIoStream, 'ImageField', "%s.jpg" % uploadedImage.name.split('.')[0],
                                         'image/jpeg', sys.getsizeof(outputIoStream), None)
    return uploadedImage


class Fakulte(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    kapakfoto = models.ImageField(upload_to="kapakfotolar/", blank=True, null=True, verbose_name=_("Kapak Fotoğrafı"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.kapakfoto:
            self.kapakfoto = compressImage(self.kapakfoto)
        super(Fakulte, self).save(*args, **kwargs)

    class Meta:
        ordering = ('baslik',)
        verbose_name= _("Fakülte")
        verbose_name_plural= _("Fakülteler")


class Bolum(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    tur = models.CharField(max_length=20, choices=ALTBIRIM_TUR_CHOICES, blank=True, null=True, verbose_name=_("Tür"))
    bolum = models.ForeignKey(Fakulte, on_delete=models.CASCADE, verbose_name=_("Bölüm"))
    yazi = RichTextField(blank=True, null=True, verbose_name=_("Yazı"))
    resim = models.ImageField(upload_to='altbirimler', blank=True, null=True, verbose_name=_("Resim"))
    thumbnail = models.ImageField(upload_to="thumbnails/", blank=True, null=True, verbose_name=_("Küçük Resim"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))
    telefon = models.CharField(max_length=50, blank=True, null=True, verbose_name=_("Telefon"))
    bolum_mail = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Bölüm Mail Adresi"))
    fakulte_mail = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Fakülte Mail Adresi"))
    web_sitesi = models.CharField(max_length=200, blank=True,null=True, verbose_name=_("Web Sitesi"))
    facebook_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Facebook Hesabı"))
    instagram_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("İnstagram Hesabı"))
    twitter_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Twitter Hesabı"))
    youtube_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Youtube Hesabı"))
    linkedin_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Linkedin Hesabı"))
    youtube_video = models.TextField(blank=True, null=True, verbose_name=_("Youtube Videosu"))

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        if self.thumbnail:
            self.thumbnail = compressImage(self.thumbnail)

        super(Bolum, self).save(*args, **kwargs)
      
    class Meta:
        ordering = ('baslik',)
        verbose_name = _("Bölüm")
        verbose_name_plural = _("Bölümler")


class Enstitu(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))

    def __str__(self):
        return self.baslik

    class Meta:
        ordering = ('baslik',)
        verbose_name=_("Enstitü")
        verbose_name_plural=_("Enstitüler")


class AnaBilimDali(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    tur = models.CharField(max_length=20, choices=ALTBIRIM_TUR_CHOICES, blank=True, null=True, verbose_name=_("Tür"))
    bolum = models.ForeignKey(Enstitu, on_delete=models.CASCADE, verbose_name=_("Bölüm"))
    yazi = RichTextField(blank=True, null=True, verbose_name=_("Yazı"))
    resim = models.ImageField(upload_to='anabilimdali', blank=True, null=True, verbose_name=_("Resim"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))
    telefon = models.CharField(max_length=50, blank=True, null=True, verbose_name=_("Telefon"))
    anabilimdali_mail = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Anabilim Dalı Email Adresi"))
    web_sitesi = models.CharField(max_length=200, blank=True,null=True, verbose_name=_("Web Sitesi"))
    enstitu_mail = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Enstitü Mail Adresi"))
    facebook_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Facebook Hesabı"))
    instagram_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("İnstagram Hesabı"))
    twitter_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Twitter Hesabı"))
    youtube_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Youtube Hesabı"))
    linkedin_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Linkedin Hesabı"))
    youtube_video = models.TextField(blank=True, null=True, verbose_name=_("Youtube Videosu"))

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        super(AnaBilimDali, self).save(*args, **kwargs)

    class Meta:
        ordering = ('baslik',)
        verbose_name=_("Ana Bilim Dalı")
        verbose_name_plural=_("Ana Bilim Dalları")


class Kulup(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    yazi = RichTextField(blank=True, null=True, verbose_name=_("Yazı"))
    paragraph = RichTextField(blank=True, null=True, verbose_name=_("Paragraf"))
    resim = models.ImageField(upload_to='kulupler', blank=True, null=True, verbose_name=_("Resim"))
    logo = models.ImageField(upload_to="kulupler/logolar/", blank=True, null=True, verbose_name=_("Logo"))
    kulup_baskani = models.CharField(max_length=150, blank=True, null=True, verbose_name=_("Kulüp Başkanı"))
    kulup_danismani = models.CharField(max_length=150, blank=True, null=True, verbose_name=_("Kulüp Danışmanı"))
    kulup_danismani_link = models.CharField(max_length=150, blank=True, null=True, verbose_name=_("Kulüp Danışmanı Link"))
    kulup_mail = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Kulüp Mail Adresi"))
    web_sitesi = models.CharField(max_length=200, blank=True,null=True, verbose_name=_("Web Sitesi"))
    facebook_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Facebook Hesabı"))
    instagram_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("İnstagram Hesabı"))
    twitter_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Twitter Hesabı"))
    youtube_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Youtube Hesabı"))
    linkedin_hesap = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Linkedin Hesabı"))
    youtube_video = models.TextField(blank=True, null=True, verbose_name=_("Youtube Videosu"))
    

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        super(Kulup, self).save(*args, **kwargs)

    class Meta:
        ordering = ('baslik',)
        verbose_name=_("Kulüp")
        verbose_name_plural=_("Kulüpler")

YORUM_CHOICES = (
    ('ogretimuyesi', 'Öğretim Üyesi'),
    ('mezun', 'Mezun'),
    ('ogrenci', 'Öğrenci')
)

class Yorum(models.Model):
    adsoyad = models.CharField(max_length=100, verbose_name=_("Adı Soyadı"))
    yazi = RichTextField(blank=True, null=True, verbose_name=_("Yorum"))
    rol = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("Rolü"))
    gorev = models.CharField(max_length=20, choices=YORUM_CHOICES, blank=True, null=True, verbose_name=_("Görevi"))
    fakulte =  models.ForeignKey(Fakulte, blank=True, null=True, on_delete=models.SET_NULL, verbose_name=_("Fakültesi"))
    bolum = models.ForeignKey(Bolum, blank=True, null=True, on_delete=models.SET_NULL, verbose_name=_("Bölümü"))
    resim = models.ImageField(upload_to="yorumlar", blank=True, null=True, verbose_name=_("Resim"))

    def __str__(self):
        return self.adsoyad

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        super(Yorum, self).save(*args, **kwargs)

    class Meta:
        verbose_name=_("Yorum")
        verbose_name_plural=_("Yorumlar")

class MeslekYO(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    kapakfoto = models.ImageField(upload_to="kapakfotolar/", blank=True, null=True, verbose_name=_("Kapak Fotoğrafı"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.kapakfoto:
            self.kapakfoto = compressImage(self.kapakfoto)
        super(MeslekYO, self).save(*args, **kwargs)

    class Meta:
        ordering = ('baslik',)
        verbose_name=_("Meslek Yüksekokulu")
        verbose_name_plural=_("Meslek Yüksekokulları")


class MYOBolum(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    tur = models.CharField(max_length=20, choices=ALTBIRIM_TUR_CHOICES, blank=True, null=True, verbose_name=_("Tür"))
    myo = models.ForeignKey(MeslekYO, on_delete=models.CASCADE, verbose_name=_("Meslek Yüksekokulu"))
    yazi = RichTextField(blank=True, null=True, verbose_name=_("Yazı"))
    resim = models.ImageField(upload_to='myobolum', blank=True, null=True, verbose_name=_("Resim"))
    thumbnail = models.ImageField(upload_to="thumbnails/", blank=True, null=True, verbose_name=_("Küçük Resim"))
    sunum = models.FileField(upload_to="sunumlar/", blank=True, null=True, verbose_name=_("Sunum"))


    def __str__(self):
        return self.baslik

    class Meta:
        ordering = ('baslik',)
        verbose_name =_("MYO Bölümü")
        verbose_name_plural =_("MYO Bölümleri")

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        if not self.id and self.thumbnail:
            self.thumbnail = compressImage(self.thumbnail)
        super(MYOBolum, self).save(*args, **kwargs)



class KampusGaleri(models.Model):
    baslik = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("Başlık"))
    resim = models.ImageField(upload_to="kampusteyasam/", verbose_name=_("Resim"))


    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        super(KampusGaleri, self).save(*args, **kwargs)

    class Meta:
        verbose_name=_("Kampüs Galeri Resmi")
        verbose_name_plural=_("Kampüs Galeri Resimleri")


class OgrenciProje(models.Model):
    baslik = models.CharField(max_length=100, verbose_name=_("Başlık"))
    yazi = models.TextField(blank=True, null=True, verbose_name=_("Yazı"))
    resim = models.ImageField(upload_to='ogrenciprojeleri/', blank=True, null=True, verbose_name=_("Resim"))

    def __str__(self):
        return self.baslik

    def save(self, *args, **kwargs):
        if not self.id and self.resim:
            self.resim = compressImage(self.resim)
        super(OgrenciProje, self).save(*args, **kwargs)

    class Meta:
        ordering = ('baslik',)
        verbose_name =_("Öğrenci Projesi")
        verbose_name_plural =_("Öğrenci Projeleri")

class MezunSesleniyorVideo(models.Model):
    caption = models.CharField(max_length=100, verbose_name=_("Başlık"))
    video = models.FileField(upload_to="video/mezun_sesleniyor/%y", verbose_name=_("Video"))

    def __str__(self):
        return self.caption


class OgrenciProjeSesleniyorVideo(models.Model):
    caption = models.CharField(max_length=100, verbose_name=_("Başlık"))
    video = models.FileField(upload_to="video/ogrenciproje_sesleniyor/%y", verbose_name=_("Video"))

    def __str__(self):
        return self.caption

class OgrenciTemsilciSesleniyorVideo(models.Model):
    caption = models.CharField(max_length=100, verbose_name=_("Başlık"))
    video = models.FileField(upload_to="video/ogrencitemsilci_sesleniyor/%y", verbose_name=_("Video"))

    def __str__(self):
        return self.caption



