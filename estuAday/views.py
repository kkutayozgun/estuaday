from django.views.generic import TemplateView
from siteapp.models import *


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['yorumlar_ogrenci'] = Yorum.objects.filter(gorev='ogrenci')[:4]
        context['yorumlar_mezun'] = Yorum.objects.filter(gorev='mezun')[:4]
        context['yorumlar_ogretimuyesi'] = Yorum.objects.filter(gorev='ogretimuyesi')[:4]
        context['mezunsesleniyor_videos'] = MezunSesleniyorVideo.objects.all()
        context['ogrenciprojesesleniyor_videos'] = OgrenciProjeSesleniyorVideo.objects.all()
        context['ogrencitemsilcisesleniyor_videos'] = OgrenciTemsilciSesleniyorVideo.objects.all()
        return context